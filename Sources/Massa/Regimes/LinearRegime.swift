import Foundation

public struct LinearRegime: MassaRegime {
    public let x0: Date
    public let y0: Double
    public init(x0: Date, y0: Double, dx: TimeInterval = 86400, dy: Double) {
        self.x0 = x0
        self.y0 = y0
        self.dx = dx
        self.dy = dy
    }
    public var y: (Date) -> Double {
        get {
            func y(_ x: Date) -> Double {
                y0 + (dy * (x0.timeIntervalSince(x)) / dx)
            }
            return y
        }
    }
    let dx: TimeInterval
    let dy: Double
}
