import Foundation
import Logging

open class Massa {
    public enum Error: Swift.Error {
        case paramaterRanger
    }
    public struct Sample: Equatable {
        let date: Date
        let value: Double
        init(date: Date = Date(), value: Double) {
            self.date = date
            self.value = value
        }
    }
    private var logger = Logger(label: "net.villewitt.massa.massa")
    private let queue: DispatchQueue
    public init(queue: DispatchQueue) {
        self.queue = queue
    }
    public func add(sample: Sample) -> Result<Int,Swift.Error> {
        var theResult: Result<Int,Swift.Error>!
        add(sample: sample) { result in
            theResult = result
        }
        queue.sync {}
        return theResult
    }
    public func get(from: Date, to: Date = Date()) -> Result<[Sample],Swift.Error> {
        var theResult: Result<[Sample],Swift.Error>!
        get(from: from, to: to) { result in
            theResult = result
        }
        queue.sync {}
        return theResult
    }
    internal var samples = [Sample]()
    public func add(sample: Sample, completion: @escaping (Result<Int,Swift.Error>) -> Void) {
        samples.append(sample)
        completion(.success(0))
    }
    public func get(from: Date, to: Date = Date(), completion: @escaping (Result<[Sample],Swift.Error>) -> Void) {
        guard from < to else {
            completion(.failure(Error.paramaterRanger))
            return
        }
        let result = samples.filter({from < $0.date && $0.date <= to})
        completion(.success(result))
    }
}
