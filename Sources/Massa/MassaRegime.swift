import Foundation

public protocol MassaRegime {
    var x0: Date { get }
    var y0: Double { get }
    var y: ((Date) -> Double) { get }
    init(x0: Date, y0: Double, dx: TimeInterval, dy: Double)
}
