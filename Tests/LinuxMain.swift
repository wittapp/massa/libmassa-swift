import XCTest

import MassaTests

var tests = [XCTestCaseEntry]()
tests += MassaTests.allTests()
XCTMain(tests)
