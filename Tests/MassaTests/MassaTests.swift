import XCTest
@testable import Massa

final class MassaTests: XCTestCase {
    var sut: Massa!
    var queue: DispatchQueue!
    static let samples = [
        Massa.Sample(date: ISO8601DateFormatter().date(from: "2020-10-03T07:30:29+02:00")!, value: 80.5),
        Massa.Sample(date: ISO8601DateFormatter().date(from: "2020-10-03T08:01:23+02:00")!, value: 80.4),
        Massa.Sample(date: ISO8601DateFormatter().date(from: "2020-10-03T08:34:56+02:00")!, value: 80.2),
        Massa.Sample(date: ISO8601DateFormatter().date(from: "2020-10-03T08:56:45+02:00")!, value: 80.3),
        Massa.Sample(date: ISO8601DateFormatter().date(from: "2020-10-03T09:03:31+02:00")!, value: 80.4),
    ]
    override func setUp() {
        queue = DispatchQueue(label: #function)
        sut = Massa(queue: queue)
    }
    func testInit() {
        XCTAssertEqual(sut.samples.count, 0)
    }
    func testAddAsync() {
        var counter = 0
        for i in 0...4 {
            sut.add(sample: MassaTests.samples[i]) {
                switch $0 {
                case .success:
                    counter = counter + 1
                    break
                case .failure(let error):
                    XCTFail("Async add failed(\(error))")
                }
            }
        }
        queue.sync{}
        for i in 0...4 {
            XCTAssertEqual(sut.samples[i], MassaTests.samples[i])
        }
        XCTAssertEqual(counter, 5)
        XCTAssertEqual(sut.samples.count, counter)
    }
    func testGetAsync() {
        MassaTests.samples.forEach {
            sut.add(sample: $0) {
                switch $0 {
                case .success:
                    break
                case .failure(let error):
                    XCTFail("Preparing failed. \(error)")
                }
            }
        }
        queue.sync {}
        let from = ISO8601DateFormatter().date(from: "2020-10-03T08:00:00+02:00")!
        let to = ISO8601DateFormatter().date(from: "2020-10-03T09:00:00+02:00")!
        var result: Result<[Massa.Sample], Swift.Error>!
        sut.get(from: from, to: to) { result = $0 }
        queue.sync {}
        switch result {
        case .success(let samples):
            XCTAssertEqual(samples.count, 3)
            break
        case .failure(let error):
            XCTFail("Async get failed(\(error))")
        case .none:
            XCTFail("Result was not returned prior to test.")
        }
        sut.get(from: to, to: from) { result = $0 }
        queue.sync {}
        switch result {
        case .success(let samples):
            XCTFail("Async get returned success in negative test case \(samples)")
        case .failure:
            break
        case .none:
            XCTFail("Result was not returned prior to test.")
        }
    }
    func testAddSync() throws {
        var counter = 0
        for i in 0...4 {
            _ = try sut.add(sample: MassaTests.samples[i]).get()
            counter = counter + 1
            XCTAssertEqual(sut.samples.count, counter)
            XCTAssertEqual(sut.samples[i], MassaTests.samples[i])
        }
        XCTAssertEqual(sut.samples.count, 5)
    }
    func testGetSync() throws {
        try MassaTests.samples.forEach { _ = try sut.add(sample: $0).get() }
        let from = ISO8601DateFormatter().date(from: "2020-10-03T08:00:00+02:00")!
        let to = ISO8601DateFormatter().date(from: "2020-10-03T09:00:00+02:00")!
        var result = [Massa.Sample]()
        result = try sut.get(from: from, to: to).get()
        XCTAssertEqual(result.count, 3)
        XCTAssertThrowsError(try sut.get(from: to, to: from).get())
    }
    
    static var allTests = [
        ("testInit", testInit),
        ("testAddAsync", testAddAsync),
        ("testGetAsync", testGetAsync),
        ("testAddSync", testAddSync),
        ("testGetSync", testGetSync),
    ]
}
